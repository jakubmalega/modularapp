﻿namespace Common
{
    public interface IConfigurationProvider
    {
        string GetValueFromConfiguration(string section, string key);
    }
}