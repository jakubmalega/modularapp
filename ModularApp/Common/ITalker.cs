﻿namespace Common
{
    public interface ITalker
    {
        void Talk();
    }
}
