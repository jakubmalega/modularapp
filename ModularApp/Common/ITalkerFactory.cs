﻿namespace Common
{
    public interface ITalkerFactory
    {
        ITalker CreateTalker<T>() where T : ITalker;
    }
}