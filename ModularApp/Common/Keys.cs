﻿namespace Common
{
    public static class Keys
    {
        public static string MsmqHostKey => "Host";
        public static string MsmqProtocolKey => "Protocol";
        public const string ActivemqHostKey = "Host";
    }
}