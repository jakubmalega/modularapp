﻿namespace Common
{
    public static class Sections
    {
        public static string MsmqSection => "MSMQ";
        public static string ActivemqSection => "ActiveMQ";
    }
}