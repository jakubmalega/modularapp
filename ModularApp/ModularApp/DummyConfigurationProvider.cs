﻿using System;
using Common;

namespace ModularApp
{
    public class DummyConfigurationProvider : IConfigurationProvider
    {
        public string GetValueFromConfiguration(string section, string key)
        {
            if (section == Sections.MsmqSection)
            {
                if (key == Keys.MsmqHostKey)
                {
                    return "MsmqHost";
                }
                else if(key == Keys.MsmqProtocolKey)
                {
                    return "MsmqProtocol";
                }
            }
            else if (section == Sections.ActivemqSection)
            {
                if (key == Keys.ActivemqHostKey)
                {
                    return "ActivemqHost";
                }
            }

            return string.Empty;
        }
    }
}