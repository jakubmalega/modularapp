﻿using Autofac;
using Autofac.Core;
using Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ModularApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var builder = CreateContainer();
            
            // builder.Resolve<ITalkersAgregate>().Talk();

            Console.ReadKey();
        }

        private static IContainer CreateContainer()
        {
            var builder = new ContainerBuilder();

            var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            if (String.IsNullOrWhiteSpace(path))
            {
                // return;
            }

            var assemblies = Directory.GetFiles(path, "Module*.dll", SearchOption.TopDirectoryOnly)
                .Select(Assembly.LoadFrom);

            foreach (var assembly in assemblies)
            {
                var modules = assembly.GetTypes()
                                      .Where(p => typeof(IModule).IsAssignableFrom(p)
                                                  && !p.IsAbstract)
                                      .Select(p => (IModule)Activator.CreateInstance(p));

                foreach (var module in modules)
                {
                    builder.RegisterModule(module);
                }
            }

            builder.RegisterType<DummyConfigurationProvider>().As<IConfigurationProvider>();

            builder.RegisterType<TalkersAgregate>().As<ITalkersAgregate>();

            return builder.Build();
        }
    }
}
