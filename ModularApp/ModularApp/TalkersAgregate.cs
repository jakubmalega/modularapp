﻿using Common;
using System;
using System.Collections.Generic;

namespace ModularApp
{
    class TalkersAgregate : ITalkersAgregate
    {
        private readonly IEnumerable<ITalker> _registeredTalkers;

        public TalkersAgregate(IEnumerable<ITalker> registeredTalkers)
        {
            _registeredTalkers = registeredTalkers;
        }

        public void Talk()
        {
            foreach(var talker in _registeredTalkers)
            {
                // Console.WriteLine(talker.Talk());
            }
        }
    }
}
