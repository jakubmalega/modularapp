﻿using Autofac;
using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModuleA
{
    public class ModA : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(c => new TalkerAFactory(c.Resolve<IConfigurationProvider>()).CreateTalker<TalkerA>())
                .As<ITalker>();
        }
    }
}
