﻿using Common;
using System;

namespace ModuleA
{
    public class TalkerA : ITalker
    {
        private readonly string _host;

        public TalkerA(string host)
        {
            _host = host;
        }

        public void Talk()
        {
            Console.WriteLine($"A {_host}");
        }
    }
}
