﻿using System;
using Common;

namespace ModuleA
{
    public class TalkerAFactory : ITalkerFactory
    {
        private readonly IConfigurationProvider _configurationProvider;

        public TalkerAFactory(IConfigurationProvider configurationProvider)
        {
            _configurationProvider = configurationProvider;
        }

        public ITalker CreateTalker<T>() where T : ITalker
        {
            string host = _configurationProvider.GetValueFromConfiguration(Sections.ActivemqSection, Keys.ActivemqHostKey);

            return new TalkerA(host);
        }
    }
}