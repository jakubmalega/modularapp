﻿using Autofac;
using Common;

namespace ModuleB
{
    public class ModB : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(c => new TalkerBFactory(c.Resolve<IConfigurationProvider>()).CreateTalker<TalkerB>())
                .As<ITalker>()
                .AutoActivate();
        }
    }
}
