﻿using System;
using System.Threading.Tasks;
using Common;
using System.Threading;

namespace ModuleB
{
    public class TalkerB : ITalker
    {
        private readonly string _host;

        private readonly string _protocol;

        private Timer m_timer;

        private readonly TaskScheduler _taskScheduler;

        public TalkerB(string host, string protocol)
        {
            _host = host;

            _protocol = protocol;

            m_timer = new Timer(x => Talk(), null, 0, 5 * 1000);
        }

        public void Talk()
        {
            Console.WriteLine($"B {_host} {_protocol}");
        }
    }
}
