﻿using Common;

namespace ModuleB
{
    public class TalkerBFactory : ITalkerFactory
    {
        private readonly IConfigurationProvider _configurationProvider;

        public TalkerBFactory(IConfigurationProvider configurationProvider)
        {
            _configurationProvider = configurationProvider;
        }

        public ITalker CreateTalker<T>() where T : ITalker
        {
            string host = _configurationProvider.GetValueFromConfiguration(Sections.MsmqSection, Keys.MsmqHostKey);
            string protocol = _configurationProvider.GetValueFromConfiguration(Sections.MsmqSection, Keys.MsmqProtocolKey);

            return new TalkerB(host, protocol);
        }
    }
}